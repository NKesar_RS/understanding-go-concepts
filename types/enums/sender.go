package enums

type Sender int64

const (
	User   Sender = iota
	Bot    Sender = iota
	System Sender = iota
)
