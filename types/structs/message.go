package structs

import (
	"UnderstandingGoConcepts/types/enums"
)

type Message struct {
	Message    string
	SenderType enums.Sender
}
