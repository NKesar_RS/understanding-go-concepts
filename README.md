# Understanding Go Concepts
This repo is used as a place for the source code of a solution for the following [medium article](https://medium.com/@nilesh.kesar/understand-concepts-in-golang-9f0195adf165)

## To run code
1. open up your terminal and navigate to the projects root dir
1. Run `go run main.go`
