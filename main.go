package main

import (
	"UnderstandingGoConcepts/types/enums"
	"UnderstandingGoConcepts/types/structs"
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

var messages chan structs.Message = make(chan structs.Message)

func printToScreen() {
	var msg structs.Message
	for {
		msg = <-messages

		if msg.SenderType == enums.Bot {
			fmt.Println(msg.Message)
		} else if msg.SenderType == enums.User {
			fmt.Print("-> ")
		} else {
			fmt.Println(msg.Message)
		}

	}
}

func handleMessage(chat string) bool {
	if strings.Contains(strings.ToLower(chat), "echo ") {
		reply := strings.Replace(strings.ToLower(chat), "echo ", "", -1)
		messages <- structs.Message{
			Message:    reply,
			SenderType: enums.Bot,
		}

	}

	if strings.ToLower(chat) == "exit" {
		return false
	}

	return true
}

func main() {
	go printToScreen()

	message := structs.Message{
		Message: "Hello, Welcome to the game \"echo\"\nType \"echo \" followed by whatever you " +
			"want repeated\nType exit by itself if you want to quit the game\n" +
			"------------------------------------------------------------------\n",
		SenderType: enums.System,
	}

	messages <- message

	reader := bufio.NewReader(os.Stdin)

	isActiveGame := true
	for isActiveGame {
		messages <- structs.Message{
			Message:    "",
			SenderType: enums.User,
		}

		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\n", "", -1)

		isActiveGame = handleMessage(text)
	}
	messages <- structs.Message{
		Message:    "You have quit the game thanks for playing and goodbye!",
		SenderType: enums.System,
	}

	time.Sleep(time.Second * 1)
}
